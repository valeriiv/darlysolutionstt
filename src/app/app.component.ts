import { AuthService } from './shared/auth.service';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'darlysolutionstt';
  topics: Observable<any[]> = null;
  user = null;

  constructor(public db: AngularFireDatabase, private as: AuthService) {}

  ngOnInit(){
    this.as.getAuthState().subscribe(user => {
      this.user = user;
    });
    this.topics = this.db.list('/informations').valueChanges();
  }

  loginWithGoogle() {
    this.as.loginWithGoogle();
  }

  isLoggedIn() {
    return this.as.isLoggedIn();
  }

  logout() {
    this.as.logout();
  }
}
