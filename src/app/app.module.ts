import { AuthService } from './shared/auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FormsModule } from '@angular/forms';

export const firebaseConfig = {
  apiKey: "AIzaSyAqh6EE8NsQ-3o9zjfS8ZHW1gR3LvgRnFk",
  authDomain: "darlysolutions.firebaseapp.com",
  databaseURL: "https://darlysolutions.firebaseio.com",
  projectId: "darlysolutions",
  storageBucket: "darlysolutions.appspot.com",
  messagingSenderId: "1047148439373",
  appId: "1:1047148439373:web:b9a32cd3a83df59b"
};
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    FormsModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
